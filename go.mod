module gitlab.com/yakshaving.art/sisi

go 1.12

require (
	github.com/gorilla/websocket v1.4.0 // indirect
	github.com/hashicorp/nomad/api v0.0.0-20190523152228-f9dbcd6e9e9a
	github.com/prometheus/client_golang v0.9.3
	github.com/sirupsen/logrus v1.4.2
)
