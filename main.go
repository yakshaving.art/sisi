package main

import (
	"flag"
	"fmt"
	"net/http"
	"os"
	"sort"
	"strings"
	"time"

	"gitlab.com/yakshaving.art/sisi/version"

	"github.com/hashicorp/nomad/api"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	log "github.com/sirupsen/logrus"
)

func main() {
	buildInfo.WithLabelValues(version.Version, version.Commit, version.Date).Set(1)
	bootTime.Set(float64(time.Now().Unix()))

	listenAddress := flag.String("listen-address", ":2112", "address in which to listen for metrics")
	metricsPath := flag.String("metrics", "/metrics", "path in which to mount prometheus metrics")
	nomadServer := flag.String("nomad-address", "http://localhost:4646", "address of the nomad server")
	pollingTime := flag.Int("poll-seconds", 30, "time in seconds that the api request is going to block on")

	debugMode := flag.Bool("debug", false, "enable debug mode")
	versionMode := flag.Bool("version", false, "print version and exit")

	enabledServices := flag.String("services", "", "comma separated job names which will be handled")
	yoloMode := flag.Bool("yolo-fuckit-i-know-what-im-doing-and-i-dont-care-who-owns-what", false, "Yolo mode promotes every service it sees")

	flag.Parse()

	if *debugMode {
		log.SetLevel(log.DebugLevel)
	}
	if *versionMode {
		fmt.Println(version.GetVersion())
		os.Exit(0)
	}

	http.HandleFunc("/status", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
	})
	http.Handle(*metricsPath, promhttp.Handler())
	go func() {
		http.ListenAndServe(*listenAddress, nil)
	}()

	c, err := api.NewClient(&api.Config{
		Address: *nomadServer,
	})
	if err != nil {
		log.Fatalf("Failed while creating nomad client: %s", err)
	}

	var filter jobFilter
	if *yoloMode {
		filter = yoloJobFilter{}
		yoloModeGauge.Set(1)
	} else {
		filter = newJobFilter(*enabledServices)
		serviceModeGauge.WithLabelValues(*enabledServices).Set(1)
	}

	n := &nomadClient{
		Client:    c,
		WaitIndex: 0,
		WaitTime:  time.Duration(*pollingTime) * time.Second,
		filter:    filter,
	}

	n.ListenForever()
}

type nomadClient struct {
	Client    *api.Client
	WaitIndex uint64
	WaitTime  time.Duration
	filter    jobFilter
}

func (n *nomadClient) ListenForever() {
	log.Infof("Starting processing of nomad tasks with filtering %s", n.filter)

	ch := make(chan *api.Deployment)
	go func() {
		log.Debugf("Fetching deployments...")
		for {
			if err := n.fetchRunningDeployments(ch); err != nil {
				log.Fatal(err)
			}
		}
	}()

	for d := range ch {
		if err := n.handleDeployment(d); err != nil {
			log.Errorf("failed to handle deployment %s: %s", d.ID, err)
		}
	}
}

func (n *nomadClient) fetchRunningDeployments(ch chan *api.Deployment) error {
	deployments, meta, err := n.Client.Deployments().List(&api.QueryOptions{
		WaitTime:  n.WaitTime,
		WaitIndex: n.WaitIndex,
	})
	if err != nil {
		return fmt.Errorf("failed to list deployments: %s", err)
	}

	queriesSummary.WithLabelValues("deployments").Observe(meta.RequestTime.Seconds())
	n.setWaitIndex(meta.LastIndex)

	deploymentsGauge.Reset()
	for _, d := range deployments {
		deploymentsGauge.WithLabelValues(d.Status).Inc()
		if d.Status == "running" {
			log.Debugf("Found suitable deployment %s in %s status", d.ID, d.Status)
			ch <- d
		}
	}
	return nil
}

func (n *nomadClient) setWaitIndex(i uint64) {
	n.WaitIndex = i
}

func (n *nomadClient) handleDeployment(d *api.Deployment) error {
	if !n.filter.shouldHandle(d.JobID) {
		log.Debugf("Ignoring job %s because it's not enabled", d.JobID)
		ignoredDeployments.WithLabelValues(d.JobID).Inc()
		return nil
	}

	log.Debugf("Handing deployment %s/%s", d.ID, d.JobID)
	for _, t := range d.TaskGroups {
		log.Debugf("Taskgroup for deployment %s/%s (promoted %t) has %d desired canaries; %d placed, %d healthy, and %d unhealthy allocs",
			d.ID, d.JobID, t.Promoted, t.DesiredCanaries, t.PlacedAllocs, t.HealthyAllocs, t.UnhealthyAllocs)

		if t.Promoted == true {
			log.Debugf("Ignoring deploymet %s/%s because it is promoted already", d.ID, d.JobID)
			return nil
		}

		if t.HealthyAllocs == t.DesiredCanaries {
			log.Debugf("Promoting deployment %s/%s because we reached the number of desired canaries %d", d.ID, d.JobID, t.DesiredCanaries)

			_, m, err := n.Client.Deployments().PromoteAll(d.ID, nil)
			if err != nil {
				return fmt.Errorf("Errd when promoting deployment %s/%s: %s", d.ID, d.JobID, err)
			}

			log.Debugf("Deployment %s/%s correctly promoted", d.ID, d.JobID)
			promotedDeployments.WithLabelValues(d.JobID).Inc()
			queriesSummary.WithLabelValues("promote").Observe(m.RequestTime.Seconds())
			n.setWaitIndex(m.LastIndex)

			return nil
		}

		if t.UnhealthyAllocs == t.DesiredCanaries {
			log.Debugf("Setting deployment %s/%s as failed because we reached the number of unhealthy allocs %d", d.ID, d.JobID, t.DesiredCanaries)

			_, m, err := n.Client.Deployments().Fail(d.ID, nil)
			if err != nil {
				return fmt.Errorf("Errd when failing deployment %s/%s: %s", d.ID, d.JobID, err)
			}

			log.Debugf("Deployment %s/%s failed due to unhealthy allocs", d.ID, d.JobID)
			failedDeployments.WithLabelValues(d.JobID).Inc()
			queriesSummary.WithLabelValues("fail").Observe(m.RequestTime.Seconds())
			n.setWaitIndex(m.LastIndex)

			return nil
		}

		log.Debugf("Ignoring deploymet %s because it does not have enough healthy allocations yet %d/%d",
			d.ID, t.HealthyAllocs, t.DesiredCanaries)
	}
	return nil
}

type jobFilter interface {
	shouldHandle(service string) bool
}

type yoloJobFilter struct{}

func (yoloJobFilter) shouldHandle(string) bool {
	return true
}

func (yoloJobFilter) String() string {
	return "enabled in YOLO mode, thus promoting everything"
}

type serviceJobFilter struct {
	whitelisted map[string]bool
}

func newJobFilter(servicesList string) jobFilter {
	w := make(map[string]bool)
	for _, j := range strings.Split(servicesList, ",") {
		j = strings.TrimSpace(j)
		if j != "" {
			w[j] = true
		}
	}

	if len(w) == 0 {
		log.Fatalf("No service selected to watch. Use the -service flag to set which services will be handled by sisi")
	}

	return serviceJobFilter{w}
}

func (f serviceJobFilter) shouldHandle(jobID string) bool {
	_, ok := f.whitelisted[jobID]
	return ok
}

func (f serviceJobFilter) String() string {
	jobs := make([]string, 0)
	for j := range f.whitelisted {
		jobs = append(jobs, j)
	}
	sort.Strings(jobs)

	return fmt.Sprintf("enabled for %s", strings.Join(jobs, ", "))
}
