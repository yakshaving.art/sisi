# Sisi

It does a simple thing:

1. Connects to a nomad server
2. Listens for new deployments
3. Promotes them when we have enough canaries
4. Fails them when we have as many unhealthy as desired canaries

## Not suitable for production

Use at your own risk

## Metrics

Provides prometheus metrics such as the number of deployments in different
states (gauge), promotions and failures counters

## Deploying to nomad

Sisi supports being executed within Nomad itself just by running [this
file](sisi.nomad).

Once you start executing it, it will report metrics on port 2112 in the app,
and it will promote itself as soon as it reaches healthy, providing the first
sample of metrics to explore.