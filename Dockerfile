FROM alpine:3.8

RUN apk --no-cache add ca-certificates=20190108-r0 libc6-compat=1.1.19-r11

COPY sisi /

EXPOSE 2112
 
ENTRYPOINT [ "/sisi" ]
