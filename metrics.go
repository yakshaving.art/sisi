package main

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var (
	namespace = "sisi"

	queriesSummary = promauto.NewSummaryVec(prometheus.SummaryOpts{
		Namespace: namespace,
		Name:      "query_seconds",
		Help:      "summary of observed query times",
	}, []string{"query"})

	deploymentsGauge = promauto.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: namespace,
		Name:      "deployments",
		Help:      "Number of deployments observed per status",
	}, []string{"status"})

	promotedDeployments = promauto.NewCounterVec(prometheus.CounterOpts{
		Namespace: namespace,
		Name:      "promoted_deployments_total",
		Help:      "Number of deployments promoted",
	}, []string{"jobname"})

	failedDeployments = promauto.NewCounterVec(prometheus.CounterOpts{
		Namespace: namespace,
		Name:      "failed_deployments_total",
		Help:      "Number of deployments failed",
	}, []string{"jobname"})

	ignoredDeployments = promauto.NewCounterVec(prometheus.CounterOpts{
		Namespace: namespace,
		Name:      "ignored_deployments_total",
		Help:      "Number of deployments ignored",
	}, []string{"jobname"})

	bootTime = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: namespace,
		Name:      "boot_time_seconds",
		Help:      "unix timestamp of when the service was started",
	})

	buildInfo = promauto.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: namespace,
		Name:      "build_info",
		Help:      "Build information",
	}, []string{"version", "commit", "date"})

	yoloModeGauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: namespace,
		Name:      "yolo",
		Help:      "Yolo mode enabled",
	})

	serviceModeGauge = promauto.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: namespace,
		Name:      "service_mode",
		Help:      "service mode enabled",
	}, []string{"watched_services"})
)
